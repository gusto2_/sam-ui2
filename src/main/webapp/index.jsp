<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd"> <%-- parameter page --%>
<html>
    <%@include file="header.html" %>
        <div class="yui3-skin-sam">
            <div id="mainTable"></div>
            <div id="mainTablePaginator"></div>

            <div id="flowTable"></div>
            <!-- still didn't find the code to reload the table -->
            <!-- 
            <div><button id="btnReload" class="yui3-button">Reload</button></div>
            -->

            <div id="pagBasicCCont"></div>
            
        </div>
        <script type="text/javascript">
            function displayMessage(msg)
            {
                var msgElement = document.getElementById("messages");
                msgElement.innerHTML = msg;
            }

            YUI({
//                combine: false, filter: 'raw',
//                gallery: 'gallery-2012.12.12-21-11'
            }).use("datatable-datasource", "datatable-scroll",
                    "datasource-function", "datasource-jsonschema",
                    "datasource-get", "datasource-io",
                    "event", "querystring-parse", 'io',
                    "gallery-datatable-paginator", "gallery-paginator-view",
                    function(Y) {

                        /**
                         * TODO: propagate exception on failure to the user
                         * not always successful
                         */
                        var myCallback = {
                            failure: function(e) {
                                displayMessage(e.error.message + ', ' + e.data.status + ' ' + e.data.statusText);
                            },
                        };
                        /** ---------- MAIN ACTIVITY TABLE -----------  **/
                        // TODO skip directly to the defined page
                        var pmodel = new Y.PaginatorModel({
                            itemsPerPage: 10,
                            page: <%=(request.getParameter("page") != null) ? request.getParameter("page") : "1"%>, // setting paramPage here doesn't work
                        });
                        var pagBar = new Y.PaginatorView({
                            model: pmodel,
                            container: '#mainTablePaginator',
                            pageOptions: [10, 25, 50, 100, 'All'],
                            maxPageLinks: 10,
                        });
                        pagBar.render();

                        var myDS = new Y.DataSource.IO({
                            source: '<%= application.getInitParameter("samDataResource") %>/list',
//                            source: './data.json',
                        });

                        myDS.plug(Y.Plugin.DataSourceJSONSchema,
                                {
                                    schema: {
                                        resultListLocator: "aggregated",
                                        resultFields: [
                                            'timestamp',
                                            "operation", "port",
                                            "elapsed", "types",
                                            'flowID',
                                            "providerIP",
                                            "providerHost"
                                        ],
                                        metaFields: {
                                            totalItems: "count"
                                        }
                                    }
                                });


                        var myDT = new Y.DataTable({
                            columns: [
                                {
                                    key: "flowID",
                                    formatter: function(o) {
                                        return '<a href="flow.jsp?flowId=' + o.value + '&page=1"><img src="./arrow-next.gif"/></a>'
                                    },
                                    allowHTML: true
                                },
                                {
                                    key: "timestamp",
                                    formatter: function(o) {
                                        return Y.Date.format(new Date(o.value), {format: "%x %X"});
                                    }
                                },
                                'operation', 'port', 'elapsed', "types",
                                'providerIP', 'providerHost'],
                            paginator: pagBar,
                            height: 500,
                            scrollable: 'y',
                            paginationSource: 'remote',
                            requestStringTemplate: "?page={page}&limit={itemsPerPage}",
                        });

                        var tableDS = {
                            datasource: myDS,
                            callback: myCallback,
//                                    initialRequest: '?page=1&limit=10',
                        };
                        myDT.plug(Y.Plugin.DataTableDataSource, tableDS);
                        myDT.render('#mainTable');
                        myDT.datasource.load({
                            request: '?page=1&limit=10',
                            callback: {
                                failure: function(e) {
                                    displayMessage(e.error.message + ', ' + e.data.status + ' ' + e.data.statusText);
                                },
                                success: function(e) {
                                    myDT.datasource.onDataReturnInitializeTable(e);
                                },
                            },
                        });
                    });
        </script>
    </body>
</html>
