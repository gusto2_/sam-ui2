<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd"> <%-- parameter page --%>
<html>
    <%@include file="header.html" %>
        <div><span><a href='index.jsp?page=<%=request.getParameter("page")%>'>&lt;&lt; &lt;&lt; back to index</a> </span></div>
        <div><span><a href='flow.jsp?page=<%=request.getParameter("page")%>&amp;flowId=<%=request.getParameter("flowId")%>'>&lt;&lt; back to flow</a> </span></div>
        <div><span>Flow id: <%=request.getParameter("flowId")%></span></div>
        <div><span>Event id: <%=request.getParameter("eventId")%></span></div>

        <div class="ui3-skin-sam">
            <div style="width: 600px;">
                <textarea id='messageContent' cols="72" rows="10" style='border: 1px solid; font-family: monospace'></textarea>
            </div>
        </div>
        <script type="text/javascript">
            function displayMessage(msg)
            {
                var msgElement = document.getElementById("messages");
                msgElement.innerHTML = msg;
            }

            YUI({
            }).use(
                    "datasource-jsonschema",
                    "datasource-get", "datasource-io",
                    "event", 'json-parse', 'escape',
                    function(Y) {

                        /**
                         * TODO: propagate exception on failure to user
                         */

                        /** ---------- MAIN ACTIVITY TABLE -----------  **/
                        // TODO skip directly to the defined page

                        var myDS = new Y.DataSource.IO({
                            source: '<%= application.getInitParameter("samDataResource") %>/event/<%=request.getParameter("eventId")%>'
                        });
                        myDS.plug(Y.Plugin.DataSourceJSONSchema,
                                {
                                    schema: {
//                                        resultListLocator: "events",
                                        resultFields: [
                                            'content',
                                            'type',
                                        ],
                                    }
                                });

                        myDS.sendRequest({
                            on: {
                                success: function(e) {
                                    var msgElement = document.getElementById('messageContent');
                                    var msg = Y.JSON.parse(e.data.responseText).content;
                                    msg = Y.Escape.html(msg);
                                    msgElement.innerHTML = msg;
                                },
                                failure: function(e) {
                                    displayMessage(e.error.message);
                                }
                            }
                        });
                    }
            );
        </script>
    </body>
</html>