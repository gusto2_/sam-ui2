<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd"> <%-- parameter page --%>
<html>
    <%@include file="header.html" %>   
        <div><span><a href="index.jsp?page=<%=request.getParameter("page")%>">&lt;&lt; back</a> </span></div>
        <div><span>Flow id: <%=request.getParameter("flowId")%></span></div>

        <div class="yui3-skin-sam">
            <div id="mainTable"></div>
            <script type="text/javascript">
                function displayMessage(msg)
                {
                    var msgElement = document.getElementById("messages");
                    msgElement.innerHTML = msg;
                }

                YUI({
                }).use("datatable-datasource", "datatable-scroll",
                        "datasource-function", "datasource-jsonschema",
                        "datasource-get", "datasource-io",
                        "event", "querystring-parse",
                        "gallery-datatable-paginator", "gallery-paginator-view",
                        function(Y) {

                            /**
                             * TODO: propagate exception on failure to user
                             */

                            /** ---------- MAIN ACTIVITY TABLE -----------  **/
                            // TODO skip directly to the defined page

                            var myDS = new Y.DataSource.IO({
                                source: '<%= application.getInitParameter("samDataResource") %>/flow/<%=request.getParameter("flowId")%>'
                            });

                            myDS.plug(Y.Plugin.DataSourceJSONSchema,
                                    {
                                        schema: {
                                            resultListLocator: "events",
                                            resultFields: [
                                                'id',
                                                'timestamp',
                                                'operation',
                                                'type',
                                                'ip',
                                                'principal'
                                            ],
                                        }
                                    });


                            var myDT = new Y.DataTable({
                                columns: [
                                    {
                                        key: 'id',
                                        formatter: function(o) {
                                            return '<a href="event.jsp?eventId=' + o.value + '&page=1&flowId=<%=request.getParameter("flowId")%>"><img src="./arrow-next.gif"/></a>';
                                        },
                                        allowHTML: true
                                    },
                                    {
                                        key: "timestamp",
                                        formatter: function(o) {
                                            return Y.Date.format(new Date(o.value), {format: "%x %X"});
                                        }
                                    },
                                    'operation', 'type', 'principal', 'ip'],
                                height: 500,
                                scrollable: 'y',
                            });

                            myDT.plug(Y.Plugin.DataTableDataSource, {
                                datasource: myDS,
                                initialRequest: ''
                            });

                            myDT.render('#mainTable');

                        });
            </script>            
        </div>
    </body>
</html>