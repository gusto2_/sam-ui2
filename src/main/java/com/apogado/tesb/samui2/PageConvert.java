/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apogado.tesb.samui2;

import java.text.NumberFormat;
import java.util.concurrent.Exchanger;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.camel.Exchange;

/**
 * convert the page parameter into offset
 *
 * @author Gabriel
 */
public class PageConvert {

    private static final Logger LOG = Logger.getLogger(PageConvert.class.getName());
    /**
     * list resource path
     */
    public static final String PATH_LIST = "/list";

    /**
     * convert query parameter page to offset
     *
     * @param exchange
     */
    public void convertPageToOffset(org.apache.camel.Exchange exchange) {

        // we will do it only for the /list resource, check if the path is "/list"
        String path = exchange.getIn().getHeader(
                Exchange.HTTP_PATH,
                String.class);
        if (!PATH_LIST.equals(path)) {
            return;
        }

        // if page is not provided, skip it
        String queryParam = exchange.getIn().getHeader(
                Exchange.HTTP_QUERY,
                String.class);
        if (queryParam == null || queryParam.length() == 0) // empty query string
        {
            return;
        }

        try {
            StringBuilder paramBuff = new StringBuilder();
            String[] params = queryParam.split("&");
            NumberFormat nf = NumberFormat.getIntegerInstance();
            nf.setGroupingUsed(false);
            int offset = 0;
            int limit = 10; // def. value
            int page = 1;
            for (int i = 0; i < params.length; i++) {
                String[] paramPair = params[i].split("=");
                if ("page".equals(paramPair[0])) {
                    page = nf.parse(paramPair[1]).intValue();
                    if(page<1)
                        throw new IllegalArgumentException("Page must be larger than 1");                        
                }
                else if ("limit".equals(paramPair[0]))
                {
                    limit = nf.parse(paramPair[1]).intValue();
                    if(limit<1)
                        throw new IllegalArgumentException("Limit must be larger than 0");
                }
            }
            offset = (page-1) * limit;
            paramBuff
                    .append("offset=").append(nf.format(offset))
                    .append('&')
                    .append("limit=").append(nf.format(limit));
            // copy headers
            exchange.getOut().setHeaders(exchange.getIn().getHeaders());
            exchange.getOut().setHeader(Exchange.HTTP_QUERY, paramBuff.toString());

        } catch (Exception nfe) {
            LOG.log(Level.SEVERE, "convertPageToOffset", nfe);
            exchange.setException(nfe);
        }
    }
}
